﻿namespace Loginet.Domain.Entities;

public sealed class Album: BaseEntity
{
    public string Title { get; set; } = string.Empty;
    
    public int UserId { get; set; }
    
    public User? User { get; set; }
    
}