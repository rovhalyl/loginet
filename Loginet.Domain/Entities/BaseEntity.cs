﻿namespace Loginet.Domain.Entities;

/// <summary>
/// Базовая сущность
/// </summary>
public abstract class BaseEntity: IEquatable<BaseEntity>
{
    /// <summary>
    /// Первичный ключ - уникальный идентификатор Guid
    /// </summary>
    public int Id { get; set;}
    
    /// <summary>
    /// Дата создания сущности
    /// </summary>
    public DateTimeOffset CreatedAt { get; set; }
    
    /// <summary>
    /// Дата обновления сущности
    /// </summary>
    public DateTimeOffset UpdatedAt { get; set; }
    
    /// <summary>
    /// Сравнение сущностей
    /// </summary>
    /// <param name="other">сущность, с которой сравнивают</param>
    /// <returns></returns>
    public bool Equals(BaseEntity? other) => other is not null && Id.Equals(other.Id);
    
}