﻿using System.Text.Json;
using Loginet.Core.Common.Interfaces.WebClients;
using Loginet.Core.Common.Models;
using Loginet.Infrastructure.Common.Options;
using Microsoft.Extensions.Options;

namespace Loginet.Infrastructure.WebApiClients;

public class JsonPlaceHolderClient: IJsonPlaceHolderClient
{
    private readonly HttpClient _httpClient;
    private readonly JsonPlaceHolderOptions _options;

    public JsonPlaceHolderClient(HttpClient httpClient, IOptions<JsonPlaceHolderOptions> options)
    {
        _httpClient = httpClient;
        _options = options.Value;
    }

    public async Task<List<UserModel>> GetUsersDataAsync()
    {
        List<UserModel>? models = null;
        
        var result = await _httpClient.GetAsync(_options.UsersUrl)
            .Result
            .Content
            .ReadAsStringAsync();
        models = JsonSerializer.Deserialize<List<UserModel>>(
            result, new JsonSerializerOptions {
                AllowTrailingCommas = true, 
            }
        );
        if (models is null) throw new NullReferenceException("User models is null");
        return models;
    }

    public async Task<List<AlbumModel>> GetAlbumsDataAsync()
    {
        List<AlbumModel>? models = null;
        
        var result = await _httpClient.GetAsync(_options.AlbumsUrl)
            .Result
            .Content
            .ReadAsStringAsync();
        models = JsonSerializer.Deserialize<List<AlbumModel>>(
            result, new JsonSerializerOptions {
                AllowTrailingCommas = true, 
            }
        );
        if (models is null) throw new NullReferenceException("Album models is null");
        return models;
    }
}