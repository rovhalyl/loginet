﻿using Loginet.Core.Common.Interfaces.Persistence;
using Loginet.Core.Common.Interfaces.WebClients;
using Loginet.Infrastructure.Common.Options;
using Loginet.Infrastructure.Common.Persistence;
using Loginet.Infrastructure.WebApiClients;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loginet.Infrastructure.Common.Extensions;

public static class DependencyInjectionExtensions {
    public static void InjectInfrastructure(this IServiceCollection services, IConfiguration configuration) {
        // Подключение базы
        services.AddDbContext<DBContext>(b => b
            .UseNpgsql(configuration.GetConnectionString("Database"), x =>
                x.MigrationsAssembly(System.Reflection.Assembly.GetAssembly(typeof(DBContext))!.FullName))
            .UseApplicationServiceProvider(services.BuildServiceProvider())
            .UseSnakeCaseNamingConvention()
        );
        // Подключение настроек  из конфига
        services
            .AddOptions<JsonPlaceHolderOptions>()
            .Bind(configuration.GetSection(JsonPlaceHolderOptions.SectionName));
        
        // Подключение контекста
        services.AddScoped<IDbContext>(provider => provider.GetRequiredService<DBContext>());
        
        // Клиенты внешних API
        services.AddHttpClient<IJsonPlaceHolderClient, JsonPlaceHolderClient>();

    }
    
}