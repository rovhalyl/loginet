﻿namespace Loginet.Infrastructure.Common.Options;

public class JsonPlaceHolderOptions
{
    public const string SectionName = "Info:JsonPlaceHolder";

    public string UsersUrl { get; set; } = string.Empty;

    public string AlbumsUrl { get; set; } = string.Empty;
}