﻿using Loginet.Core.Common.Interfaces.Persistence;
using Loginet.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Loginet.Infrastructure.Common.Persistence;

public sealed class DBContext : DbContext, IDbContext
{
    public DBContext(DbContextOptions<DBContext> options) : base(options)
    {}

    public DbSet<User> Users => Set<User>();
    public DbSet<Album> Albums => Set<Album>();
    
    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        modelBuilder.Entity<User>().ToTable("user")
            .HasMany(e => e.Albums)
            .WithOne(e => e.User)
            .HasForeignKey((e => e.UserId))
            .IsRequired();
        modelBuilder.Entity<Album>()
            .ToTable("album");

    }

    public override int SaveChanges(bool acceptAllChangesOnSuccess)
    {
        AddTimestamps();
        return base.SaveChanges(acceptAllChangesOnSuccess);
    }

    public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
        CancellationToken cancellationToken = new())
    {
        AddTimestamps();
        return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }

    /// <summary>
    /// Метод для инициализиц полей CreatedAt и UpdatedAt
    /// </summary>
    private void AddTimestamps()
    {
        var entities = ChangeTracker.Entries()
            .Where(x => x is { Entity: BaseEntity, State: EntityState.Added or EntityState.Modified });

        foreach (var entity in entities)
        {
            var now = DateTime.UtcNow;
            if (entity.State == EntityState.Added)
                ((BaseEntity)entity.Entity).CreatedAt = now;
            ((BaseEntity)entity.Entity).UpdatedAt = now;
        }
    }
}