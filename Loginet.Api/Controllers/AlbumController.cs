﻿using Loginet.Core.Common.Interfaces.EntityServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Loginet.Api.Controllers;

[Route("api/albums/")]
public class AlbumController: Controller
{
    private readonly IAlbumService _albumService;

    public AlbumController(IAlbumService albumService)
    {
        _albumService = albumService;
    }
    
    /// <summary>
    /// Метод для получения всех альбомов
    /// </summary>
    /// <returns></returns>
    [HttpGet("")]
    public async Task<IActionResult> GetAlbums()
    {
        return Ok(await _albumService.GetAlbums());
    }
    
    /// <summary>
    /// Метод для получения альбома по айди
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [HttpGet("{id:int}")]
    public async Task<IActionResult> GetAlbumAsync(int id) {
        return Ok(await _albumService.Entities().Include(x=>x.User).FirstOrDefaultAsync(x=>x.Id.Equals(id)) 
                  ?? throw new Exception("Album with this id not found"));
    }
}