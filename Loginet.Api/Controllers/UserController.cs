﻿using Loginet.Core.Common.Interfaces.EntityServices;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NeinLinq;

namespace Loginet.Api.Controllers;

[Route("api/users/")]
public class UserController: Controller
{
    private readonly IUserService _userService;
    
    public UserController(IUserService userService)
    {
        _userService = userService;
    }
    
    /// <summary>
    /// Метод для получения всех пользователей
    /// </summary>
    /// <returns></returns>
    [HttpGet("")]
    public async Task<IActionResult> GetUsers()
    {
        return Ok(await _userService.GetUsers());
    }
    
    /// <summary>
    /// Метод для получения пользователя по id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    [HttpGet("{id:int}")]
    public async Task<IActionResult> GetUserAsync(int id)
    {
        return Ok( await _userService.Entities().FirstOrDefaultAsync(x=>x.Id.Equals(id)) 
                  ?? throw new Exception("User with this id not found"));
    }
    
    /// <summary>
    /// Метод для получения всех альбомов пользователя, так же можно было сразу выводить все альбомы в ручке для одного пользователя( пррсто Include),
    /// но по тз этого не было
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("user_albums")]
    public IActionResult GetUserAlbumsAsync(int id) {
        return Ok( _userService.Context.Albums.Where(x => x.UserId.Equals(id)));
    } 
    
}