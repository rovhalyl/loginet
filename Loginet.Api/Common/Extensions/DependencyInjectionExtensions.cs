﻿using Loginet.Core.Common.Extensions;
using Loginet.Infrastructure.Common.Extensions;

namespace Loginet.Api.Common.Extensions;

internal static class DependencyInjectionExtensions
{
    internal static void InjectDependencies(this IServiceCollection services, IConfiguration configuration) {
        services.InjectInfrastructure(configuration);
        services.InjectCore();
    }
}