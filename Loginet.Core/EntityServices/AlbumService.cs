﻿using Loginet.Core.Common.Interfaces.EntityServices;
using Loginet.Core.Common.Interfaces.Persistence;
using Loginet.Core.Common.Interfaces.WebClients;
using Loginet.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Loginet.Core.EntityServices;

internal sealed class AlbumService: EntityService<Album>, IAlbumService
{
    private readonly IJsonPlaceHolderClient _jsonPlaceHolderClient;
    
    public AlbumService(IDbContext context, IJsonPlaceHolderClient jsonPlaceHolderClient) : base(context)
    {
        _jsonPlaceHolderClient = jsonPlaceHolderClient;
    }

    public override DbSet<Album> Entities() => Context.Albums;
    
    public async Task<IQueryable<Album>> GetAlbums()
    {
        if (Entities().AsNoTracking().ToList().Count > 0) return Entities().Include(x=>x.User).AsNoTracking();
        var models = await _jsonPlaceHolderClient.GetAlbumsDataAsync();
        foreach (var albumModel in models)
        {
            var newAlbum = new Album()
            {
                Id = albumModel.Id,
                Title = albumModel.Title,
                User = await Context.Users.FirstOrDefaultAsync(x=>x.Id.Equals(albumModel.UserId)),
                UserId = albumModel.UserId
                
            };
            Entities().Add(newAlbum);
        }
        await SaveChangesAsync();
        return Entities().Include(x=>x.User).AsNoTracking();

    }
}