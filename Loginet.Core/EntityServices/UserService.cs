﻿using Loginet.Core.Common.Interfaces.EntityServices;
using Loginet.Core.Common.Interfaces.Persistence;
using Loginet.Core.Common.Interfaces.WebClients;
using Loginet.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Loginet.Core.EntityServices;

internal sealed class UserService: EntityService<User>, IUserService
{
    private readonly IJsonPlaceHolderClient _jsonPlaceHolderClient;
    
    public UserService(IDbContext context, IJsonPlaceHolderClient jsonPlaceHolderClient) : base(context)
    {
        _jsonPlaceHolderClient = jsonPlaceHolderClient;
    }

    public override DbSet<User> Entities() => Context.Users;
    
    public async Task<IQueryable<User>> GetUsers()
    {
        if (Entities().AsNoTracking().ToList().Count > 0) return Entities().Include(x=>x.Albums).AsNoTracking();
        var models = await _jsonPlaceHolderClient.GetUsersDataAsync();
        foreach (var userModel in models)
        {
            var newUser = new User()
            {
                Id = userModel.Id,
                Name = userModel.Name,
                UserName = userModel.UserName,
                Email = userModel.Email,
                Street = userModel.Address.Street,
                Suite = userModel.Address.Suite,
                City = userModel.Address.City,
                ZipCode = userModel.Address.ZipCode,
                Lat = userModel.Address.Geo.Lat,
                Lng = userModel.Address.Geo.Lng,
                PhoneNumber = userModel.PhoneNumber,
                WebSite = userModel.WebSite,
                CompanyName = userModel.Company.Name,
                CatchPhrase = userModel.Company.CatchPhrase,
                Bs = userModel.Company.Bs
            };
            Entities().Add(newUser);
        }
        await SaveChangesAsync();
        return Entities().AsNoTracking();

    }
}