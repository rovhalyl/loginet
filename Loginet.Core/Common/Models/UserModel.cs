﻿using System.Text.Json.Serialization;

namespace Loginet.Core.Common.Models;

public record UserModel
{
    [JsonPropertyName("id")]
    public int Id;
    
    [JsonPropertyName("name")]
    public string Name { get; set; }
    
    [JsonPropertyName("email")]
    public string Email { get; set; }
    
    [JsonPropertyName("username")]
    public string UserName { get; set; }
    
    [JsonPropertyName("phone")]
    public string PhoneNumber { get; set; }
    
    [JsonPropertyName("website")]
    public string WebSite { get; set; }
    
    [JsonPropertyName("address")]
    public Address Address { get; set; }
    
    [JsonPropertyName("company")]
    public Company Company { get; set; }
    
}

public record Address
{
    [JsonPropertyName("street")]
    public string Street { get; set; }
    
    [JsonPropertyName("suite")]
    public string Suite { get; set; }
    
    [JsonPropertyName("city")]
    public string City { get; set; }
    
    [JsonPropertyName("zipcode")]
    public string ZipCode { get; set; }
    
    [JsonPropertyName("geo")]
    public Geo Geo { get; set; }
    
}

public record Geo
{
    [JsonPropertyName("lat")]
    public string Lat { get; set; }
    
    [JsonPropertyName("lng")]
    public string Lng { get; set; }
    
}

public record Company
{
    [JsonPropertyName("name")]
    public string Name { get; set; }
    
    [JsonPropertyName("catchPhrase")]
    public string CatchPhrase { get; set; }
    
    [JsonPropertyName("bs")]
    public string Bs { get; set; }
}