﻿using System.Text.Json.Serialization;

namespace Loginet.Core.Common.Models;

public record AlbumModel
{
    [JsonPropertyName("userId")]
    public int UserId { get; set; }
    
    [JsonPropertyName("id")]
    public int Id { get; set; }
    
    [JsonPropertyName("title")]
    public string Title { get; set; }

}