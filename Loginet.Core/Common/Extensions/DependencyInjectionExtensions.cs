﻿using Loginet.Core.Common.Interfaces.EntityServices;
using Loginet.Core.EntityServices;
using Microsoft.Extensions.DependencyInjection;

namespace Loginet.Core.Common.Extensions;

public static class DependencyInjectionExtensions
{
    public static void InjectCore(this IServiceCollection services)
    {
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IAlbumService, AlbumService>();
    }
}