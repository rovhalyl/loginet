﻿using Loginet.Domain.Entities;

namespace Loginet.Core.Common.Extensions;

/// <summary>
/// Класс расширения базовой сущности
/// </summary>
public static class BaseEntityExtensions {
    /// <summary>
    /// Применить сортировку по ID
    /// </summary>
    public static IQueryable<TEntity> EnsureSortingById<TEntity>(this IQueryable<TEntity> entities)
        where TEntity : BaseEntity {
        if (entities is not IOrderedQueryable<TEntity> qo)
            return entities.OrderBy(x => x.Id);
        try {
            return qo.ThenBy(x => x.Id);
        }
        catch {
            return qo.OrderBy(x => x.Id);
        }
    }

    /// <summary>
    /// Фильтрация по ID
    /// </summary>
    public static IQueryable<TBaseEntity> FilterById<TBaseEntity>(this IQueryable<TBaseEntity> entities, int id)
        where TBaseEntity : BaseEntity => entities.Where(x => x.Id.Equals(id));
}