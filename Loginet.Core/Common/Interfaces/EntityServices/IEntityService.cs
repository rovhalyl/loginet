﻿using Loginet.Core.Common.Interfaces.Persistence;
using Loginet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace Loginet.Core.Common.Interfaces.EntityServices;

public interface IEntityService<TEntity> where TEntity : BaseEntity
{
    /// <summary>
    /// A DbContext instance represents a session with the database and can be used to query and save instances of your entities.
    /// DbContext is a combination of the Unit Of Work and Repository patterns.
    /// </summary>
    /// <returns></returns>
    public IDbContext Context { get; }

    /// <summary>
    /// Метод для поиска 1 единицы сущности
    /// </summary>
    /// <param name="id">ID сущности</param>
    /// <returns></returns>
    public TEntity GetOne(int id);
    
    /// <summary>
    /// Набор основной сущности сервиса
    /// </summary>
    /// <returns></returns>
    public DbSet<TEntity> Entities();
    
    /// <summary>
    /// Starts a new transaction.
    /// </summary>
    /// <returns>A IDbContextTransaction that represents the started transaction.</returns>
    public IDbContextTransaction BeginTransaction();
    
    /// <summary>
    /// Saves all changes made in this context to the database. 
    /// </summary>
    /// <param name="acceptAllChangesOnSuccess">Indicates whether AcceptAllChanges() is called after the changes have been sent successfully to the database.</param>
    /// <returns>The number of state entries written to the database.</returns>
    public int SaveChanges(bool acceptAllChangesOnSuccess = true);
    
    /// <summary>
    /// Saves all changes made in this context to the database. 
    /// </summary>
    /// <param name="acceptAllChangesOnSuccess">Indicates whether AcceptAllChanges() is called after the changes have been sent successfully to the database.</param>
    /// <returns>A task that represents the asynchronous save operation. The task result contains the number of state entries written to the database.</returns>
    public Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess = true);
}