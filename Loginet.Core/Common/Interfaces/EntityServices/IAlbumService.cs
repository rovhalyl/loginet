﻿using Loginet.Domain.Entities;

namespace Loginet.Core.Common.Interfaces.EntityServices;

public interface IAlbumService: IEntityService<Album>
{
    public Task<IQueryable<Album>> GetAlbums();
}