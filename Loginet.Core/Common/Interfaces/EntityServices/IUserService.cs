﻿using Loginet.Domain.Entities;

namespace Loginet.Core.Common.Interfaces.EntityServices;

public interface IUserService: IEntityService<User>
{
    public Task<IQueryable<User>> GetUsers();
}