﻿using Loginet.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace Loginet.Core.Common.Interfaces.Persistence;

/// <summary>
/// Интерфейс контекста базы
/// </summary>
public interface IDbContext {
    /// <summary>
    /// Таблица пользователей
    /// </summary>
    public DbSet<User> Users {get; }
    
    /// <summary>
    /// Таблица альбомов
    /// </summary>
    public DbSet<Album> Albums { get; }
    
    /// <inheritdoc cref="DatabaseFacade" />
    public DatabaseFacade Database { get; }
    
    /// <summary>
    /// Saves all changes made in this context to the database. 
    /// </summary>
    /// <param name="acceptAllChangesOnSuccess">Indicates whether AcceptAllChanges() is called after the changes have been sent successfully to the database.</param>
    /// <returns>A task that represents the asynchronous save operation. The task result contains the number of state entries written to the database.</returns>
    public int SaveChanges(bool acceptAllChangesOnSuccess);

    /// <summary>
    /// Saves all changes made in this context to the database. 
    /// </summary>
    /// <param name="acceptAllChangesOnSuccess">Indicates whether AcceptAllChanges() is called after the changes have been sent successfully to the database.</param>
    /// <param name="cancellationToken">A <see cref="CancellationToken" /> to observe while waiting for the task to complete.</param>
    /// <returns>A task that represents the asynchronous save operation. The task result contains the number of state entries written to the database.</returns>
    public Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new());
}