﻿using Loginet.Core.Common.Models;

namespace Loginet.Core.Common.Interfaces.WebClients;

public interface IJsonPlaceHolderClient
{
    public Task<List<UserModel>> GetUsersDataAsync();

    public Task<List<AlbumModel>> GetAlbumsDataAsync();
}